/*==============================================================*/ 
/*  Un procedimiento almacenado donde se ingresa el nombre de un 
cliente y devuelva la cantidad de contratos que le pertenecen */ 
/*==============================================================*/
CREATE OR REPLACE FUNCTION CONT_CONTRATOS (character varying) RETURNS SETOF "record" 
AS
$BODY$

    SELECT CLIENTE.CLIENTE_APELLIDO ||' '|| CLIENTE.CLIENTE_NOMBRE  AS CLIENTE, COUNT(CONTRATO.CLIENTE_ID) AS NUM_CONTRATOS FROM CONTRATO
	INNER JOIN CLIENTE ON CLIENTE.CLIENTE_ID = CONTRATO.CLIENTE_ID
    WHERE CLIENTE.CLIENTE_APELLIDO = $1
    GROUP BY CLIENTE.CLIENTE_APELLIDO, CLIENTE.CLIENTE_NOMBRE
	ORDER BY CLIENTE ASC 

$BODY$

LANGUAGE SQL;

SELECT * FROM CONT_CONTRATOS ('Chavez Lopez') /*Escribir los apellidos del cliente a consultar*/
AS ("NOMBRES" character varying ,"NUM_CONTRATOS" BIGINT);
