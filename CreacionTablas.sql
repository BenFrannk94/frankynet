/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     16/11/2021 0:44:26                           */
/*==============================================================*/


/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   CLIENTE_ID           INT4                 not null,
   CLIENTE_CEDULA       VARCHAR(10)          not null,
   CLIENTE_NOMBRE       CHAR(20)             not null,
   CLIENTE_APELLIDO     CHAR(20)             not null,
   CLIENTE_NACIMIENTO   DATE                 not null,
   CLIENTE_EMAIL        VARCHAR(30)          not null,
   CLIENTE_TELEFONO_1   VARCHAR(10)          not null,
   CLIENTE_TELEFONO_2   VARCHAR(10)          null,
   CLIENTE_DIRECCION    VARCHAR(30)          null,
   constraint PK_CLIENTE primary key (CLIENTE_ID)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
CLIENTE_ID
);

/*==============================================================*/
/* Table: CONTRATO                                              */
/*==============================================================*/
create table CONTRATO (
   CONTRATO_ID          INT4                 not null,
   CLIENTE_ID           INT4                 not null,
   EMPLEADO_ID          INT4                 not null,
   SERVICIO_ID          INT4                 not null,
   CONTRATO_INICIO      DATE                 not null,
   CONTRATO_FINAL       DATE                 null,
   CONTRATO_ESTADO      CHAR(10)             not null,
   CONTRATO_DIRECCION   VARCHAR(60)          not null,
   CONTRATO_DESCRIPCION VARCHAR(30)          not null,
   constraint PK_CONTRATO primary key (CONTRATO_ID)
);

/*==============================================================*/
/* Index: CONTRATO_PK                                           */
/*==============================================================*/
create unique index CONTRATO_PK on CONTRATO (
CONTRATO_ID
);

/*==============================================================*/
/* Index: FIRMA_FK                                              */
/*==============================================================*/
create  index FIRMA_FK on CONTRATO (
CLIENTE_ID
);

/*==============================================================*/
/* Index: GESTIONA_FK                                           */
/*==============================================================*/
create  index GESTIONA_FK on CONTRATO (
EMPLEADO_ID
);

/*==============================================================*/
/* Index: IMPLICA_FK                                            */
/*==============================================================*/
create  index IMPLICA_FK on CONTRATO (
SERVICIO_ID
);

/*==============================================================*/
/* Table: CUENTA_BANCARIA                                       */
/*==============================================================*/
create table CUENTA_BANCARIA (
   CUENTA_ID            INT4                 not null,
   BANCO_ID             INT4                 not null,
   CONTRATO_ID          INT4                 not null,
   CUENTA_NUM           VARCHAR(10)          not null,
   constraint PK_CUENTA_BANCARIA primary key (CUENTA_ID)
);

/*==============================================================*/
/* Index: CUENTA_BANCARIA_PK                                    */
/*==============================================================*/
create unique index CUENTA_BANCARIA_PK on CUENTA_BANCARIA (
CUENTA_ID
);

/*==============================================================*/
/* Index: EMITE_FK                                              */
/*==============================================================*/
create  index EMITE_FK on CUENTA_BANCARIA (
BANCO_ID
);

/*==============================================================*/
/* Index: VINCULA_FK                                            */
/*==============================================================*/
create  index VINCULA_FK on CUENTA_BANCARIA (
CONTRATO_ID
);

/*==============================================================*/
/* Table: EMPLEADO                                              */
/*==============================================================*/
create table EMPLEADO (
   EMPLEADO_ID          INT4                 not null,
   TIPO_EMPLEADO_ID     INT4                 not null,
   EMPLEADO_CEDULA      VARCHAR(10)          not null,
   EMPLEADO_INICIO_LABORAL DATE                 not null,
   EMPLEADO_NOMBRE      CHAR(20)             not null,
   EMPLEADO_APELLIDO    CHAR(20)             not null,
   EMPLEADO_NACIMIENTO  DATE                 not null,
   EMPLEADO_EMAIL       VARCHAR(30)          not null,
   EMPLEADO_TELEFONO    VARCHAR(10)          not null,
   constraint PK_EMPLEADO primary key (EMPLEADO_ID)
);

/*==============================================================*/
/* Index: EMPLEADO_PK                                           */
/*==============================================================*/
create unique index EMPLEADO_PK on EMPLEADO (
EMPLEADO_ID
);

/*==============================================================*/
/* Index: ES_UN_FK                                              */
/*==============================================================*/
create  index ES_UN_FK on EMPLEADO (
TIPO_EMPLEADO_ID
);

/*==============================================================*/
/* Table: EQUIPO                                                */
/*==============================================================*/
create table EQUIPO (
   EQUIPO_ID            INT4                 not null,
   TIPO_EQUIPO_ID       INT4                 not null,
   EQUIPO_CANTIDAD      INT4                 not null,
   EQUIPO_ESTADO        VARCHAR(15)          not null,
   constraint PK_EQUIPO primary key (EQUIPO_ID)
);

/*==============================================================*/
/* Index: EQUIPO_PK                                             */
/*==============================================================*/
create unique index EQUIPO_PK on EQUIPO (
EQUIPO_ID
);

/*==============================================================*/
/* Index: ES_FK                                                 */
/*==============================================================*/
create  index ES_FK on EQUIPO (
TIPO_EQUIPO_ID
);

/*==============================================================*/
/* Table: FACTURA                                               */
/*==============================================================*/
create table FACTURA (
   FACTURA_ID           INT4                 not null,
   CUENTA_ID            INT4                 not null,
   TIPO_PAGO_ID         INT4                 not null,
   FECHA_PAGADO         DATE                 not null,
   FACTURA_TOTAL        DECIMAL              not null,
   constraint PK_FACTURA primary key (FACTURA_ID)
);

/*==============================================================*/
/* Index: FACTURA_PK                                            */
/*==============================================================*/
create unique index FACTURA_PK on FACTURA (
FACTURA_ID
);

/*==============================================================*/
/* Index: GENERA_FK                                             */
/*==============================================================*/
create  index GENERA_FK on FACTURA (
CUENTA_ID
);

/*==============================================================*/
/* Index: SE_USA_EN_FK                                          */
/*==============================================================*/
create  index SE_USA_EN_FK on FACTURA (
TIPO_PAGO_ID
);

/*==============================================================*/
/* Table: PLAN_SERV                                             */
/*==============================================================*/
create table PLAN_SERV (
   PLAN_ID              INT4                 not null,
   PLAN_DESCRIPCION     VARCHAR(60)          not null,
   PLAN_VELOCIDAD       VARCHAR(20)          not null,
   PLAN_PRECIO          DECIMAL              not null,
   constraint PK_PLAN_SERV primary key (PLAN_ID)
);

/*==============================================================*/
/* Index: PLAN_SERV_PK                                          */
/*==============================================================*/
create unique index PLAN_SERV_PK on PLAN_SERV (
PLAN_ID
);

/*==============================================================*/
/* Table: SERVICIO                                              */
/*==============================================================*/
create table SERVICIO (
   SERVICIO_ID          INT4                 not null,
   EQUIPO_ID            INT4                 null,
   PLAN_ID              INT4                 not null,
   SERVICIO_DESCRIPCION VARCHAR(50)          not null,
   constraint PK_SERVICIO primary key (SERVICIO_ID)
);

/*==============================================================*/
/* Index: SERVICIO_PK                                           */
/*==============================================================*/
create unique index SERVICIO_PK on SERVICIO (
SERVICIO_ID
);

/*==============================================================*/
/* Index: FUNCIONA_CON_FK                                       */
/*==============================================================*/
create  index FUNCIONA_CON_FK on SERVICIO (
EQUIPO_ID
);

/*==============================================================*/
/* Index: INCLUYE_FK                                            */
/*==============================================================*/
create  index INCLUYE_FK on SERVICIO (
PLAN_ID
);

/*==============================================================*/
/* Table: TIPO_BANCO                                            */
/*==============================================================*/
create table TIPO_BANCO (
   BANCO_ID             INT4                 not null,
   BANCO_NOMBRE         VARCHAR(50)          not null,
   constraint PK_TIPO_BANCO primary key (BANCO_ID)
);

/*==============================================================*/
/* Index: TIPO_BANCO_PK                                         */
/*==============================================================*/
create unique index TIPO_BANCO_PK on TIPO_BANCO (
BANCO_ID
);

/*==============================================================*/
/* Table: TIPO_EMPLEADO                                         */
/*==============================================================*/
create table TIPO_EMPLEADO (
   TIPO_EMPLEADO_ID     INT4                 not null,
   TIPO_EMPLEADO_DESCRIPCION CHAR(15)             not null,
   constraint PK_TIPO_EMPLEADO primary key (TIPO_EMPLEADO_ID)
);

/*==============================================================*/
/* Index: TIPO_EMPLEADO_PK                                      */
/*==============================================================*/
create unique index TIPO_EMPLEADO_PK on TIPO_EMPLEADO (
TIPO_EMPLEADO_ID
);

/*==============================================================*/
/* Table: TIPO_EQUIPO                                           */
/*==============================================================*/
create table TIPO_EQUIPO (
   TIPO_EQUIPO_ID       INT4                 not null,
   TIPO_EQUIPO_DESCRIPCION VARCHAR(15)          not null,
   constraint PK_TIPO_EQUIPO primary key (TIPO_EQUIPO_ID)
);

/*==============================================================*/
/* Index: TIPO_EQUIPO_PK                                        */
/*==============================================================*/
create unique index TIPO_EQUIPO_PK on TIPO_EQUIPO (
TIPO_EQUIPO_ID
);

/*==============================================================*/
/* Table: TIPO_PAGO                                             */
/*==============================================================*/
create table TIPO_PAGO (
   TIPO_PAGO_ID         INT4                 not null,
   PAGO_DESCRIPCION     VARCHAR(30)          not null,
   constraint PK_TIPO_PAGO primary key (TIPO_PAGO_ID)
);

/*==============================================================*/
/* Index: TIPO_PAGO_PK                                          */
/*==============================================================*/
create unique index TIPO_PAGO_PK on TIPO_PAGO (
TIPO_PAGO_ID
);

alter table CONTRATO
   add constraint FK_CONTRATO_FIRMA_CLIENTE foreign key (CLIENTE_ID)
      references CLIENTE (CLIENTE_ID)
      on delete restrict on update restrict;

alter table CONTRATO
   add constraint FK_CONTRATO_GESTIONA_EMPLEADO foreign key (EMPLEADO_ID)
      references EMPLEADO (EMPLEADO_ID)
      on delete restrict on update restrict;

alter table CONTRATO
   add constraint FK_CONTRATO_IMPLICA_SERVICIO foreign key (SERVICIO_ID)
      references SERVICIO (SERVICIO_ID)
      on delete restrict on update restrict;

alter table CUENTA_BANCARIA
   add constraint FK_CUENTA_B_EMITE_TIPO_BAN foreign key (BANCO_ID)
      references TIPO_BANCO (BANCO_ID)
      on delete restrict on update restrict;

alter table CUENTA_BANCARIA
   add constraint FK_CUENTA_B_VINCULA_CONTRATO foreign key (CONTRATO_ID)
      references CONTRATO (CONTRATO_ID)
      on delete restrict on update restrict;

alter table EMPLEADO
   add constraint FK_EMPLEADO_ES_UN_TIPO_EMP foreign key (TIPO_EMPLEADO_ID)
      references TIPO_EMPLEADO (TIPO_EMPLEADO_ID)
      on delete restrict on update restrict;

alter table EQUIPO
   add constraint FK_EQUIPO_ES_TIPO_EQU foreign key (TIPO_EQUIPO_ID)
      references TIPO_EQUIPO (TIPO_EQUIPO_ID)
      on delete restrict on update restrict;

alter table FACTURA
   add constraint FK_FACTURA_GENERA_CUENTA_B foreign key (CUENTA_ID)
      references CUENTA_BANCARIA (CUENTA_ID)
      on delete restrict on update restrict;

alter table FACTURA
   add constraint FK_FACTURA_SE_USA_EN_TIPO_PAG foreign key (TIPO_PAGO_ID)
      references TIPO_PAGO (TIPO_PAGO_ID)
      on delete restrict on update restrict;

alter table SERVICIO
   add constraint FK_SERVICIO_FUNCIONA__EQUIPO foreign key (EQUIPO_ID)
      references EQUIPO (EQUIPO_ID)
      on delete restrict on update restrict;

alter table SERVICIO
   add constraint FK_SERVICIO_INCLUYE_PLAN_SER foreign key (PLAN_ID)
      references PLAN_SERV (PLAN_ID)
      on delete restrict on update restrict;

