/*==============================================================*/ 
/*      1: Historico de contratos de la empresa por año         */ 
/*==============================================================*/
SELECT EXTRACT ( YEAR FROM CONTRATO.CONTRATO_INICIO) AS PERIODO, COUNT (CONTRATO.CONTRATO_ID) AS CANTIDAD_CONTRATO FROM CONTRATO
GROUP BY EXTRACT ( YEAR FROM CONTRATO.CONTRATO_INICIO) 
ORDER BY PERIODO ASC 