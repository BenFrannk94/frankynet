/*==============================================================*/ 
/*  3: Historico de ganancias anuales de parte de cada cliente  */ 
/*==============================================================*/
SELECT CLIENTE.CLIENTE_APELLIDO ||' '|| CLIENTE.CLIENTE_NOMBRE AS CLIENTE, EXTRACT ( YEAR FROM FACTURA.FECHA_PAGADO) AS PERIODO, SUM (FACTURA.FACTURA_TOTAL) AS PAGO_TOTAL FROM CLIENTE
INNER JOIN CONTRATO ON CONTRATO.CLIENTE_ID = CLIENTE.CLIENTE_ID
INNER JOIN CUENTA_BANCARIA ON CUENTA_BANCARIA.CONTRATO_ID = CONTRATO.CONTRATO_ID
INNER JOIN FACTURA ON FACTURA.CUENTA_ID = CUENTA_BANCARIA.CUENTA_ID
GROUP BY EXTRACT ( YEAR FROM FACTURA.FECHA_PAGADO), CLIENTE.CLIENTE_NOMBRE, CLIENTE.CLIENTE_APELLIDO
ORDER BY CLIENTE,PERIODO ASC 