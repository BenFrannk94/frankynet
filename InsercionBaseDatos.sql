/*==============================================================*/ 
/*      Ingreso de datos en tabla TIPO_EMPLEADO                 */ 
/*==============================================================*/
INSERT INTO TIPO_EMPLEADO VALUES
(1,'Asesor'),
(2,'Vendedor'),
(3,'ATT Cliente'),
(4,'Tecnico'),
(5,'Mantenimiento'),
(6,'Seguridad');

/*==============================================================*/ 
/*      Ingreso de datos en tabla TIPO_EQUIPO                 */ 
/*==============================================================*/
INSERT INTO TIPO_EQUIPO VALUES
(1,'ONU integrada'),
(2,'Router');

/*==============================================================*/ 
/*      Ingreso de datos en tabla TIPO_BANCO                   */ 
/*==============================================================*/
INSERT INTO TIPO_BANCO VALUES
(1,'Banco Pichincha'),
(2,'Banco de Guayaquil'),
(3,'Banco del Pacifico');

/*==============================================================*/ 
/*      Ingreso de datos en tabla TIPO_PAGO                 */ 
/*==============================================================*/
INSERT INTO TIPO_PAGO VALUES
(1,'Pago Continuo'),
(2,'Pago Reconexion');

/*==============================================================*/ 
/*      Ingreso de datos en tabla PLAN_SERV                      */ 
/*==============================================================*/
ALTER TABLE PLAN_SERV ALTER COLUMN PLAN_PRECIO SET DATA TYPE decimal;
INSERT INTO PLAN_SERV VALUES
(1,'Plan Basico','50 Mbps',28.50),
(2,'Plan Flyer','100 Mbps',39.30),
(3,'Plan Premium','300 Mbps',69.45);

/*==============================================================*/ 
/*      Ingreso de datos en tabla CLIENTE                      */ 
/*==============================================================*/
SET DATESTYLE TO 'European';
INSERT INTO CLIENTE VALUES
(1150,'1384895137','Josefina Karla','Gomez Michelena','14/06/1985','josefina85@gmail.com','0939636536','0915468135','Jocay - Calle J7'),
(1151,'1387913548','Carolina Fernanda','Chavez Lopez','23/11/1965','carofer23@gmail.com','0928546481','0915468135','4 de noviembre - Manzana'),
(1152,'1387593108','Jose Jose','Sanchez Lopez','12/10/1995','josesanchez12@gmail.com','0935814651','0928546481','5 de junio - Calle Cepeda'),
(1153,'1598713548','Luis Fernando','Palma Mise','03/12/2000','nandomise@gmail.com','0998493168','','Miraflores - Calle 215'),
(1154,'0800819831','Janet Lilly','Sacon Cagua','20/06/1960','janetsacon@gmail.com','0915468135','','4 de noviembre - Callejon 17B'),
(1155,'0801138694','Carlos Alfredo','Gonzalez Altafuya','20/10/1962','carlosgonzalez@gmail.com','0994901714','0998493168','8 de Enero - Calle 6');

/*==============================================================*/ 
/*      Ingreso de datos en tabla EQUIPO                        */ 
/*==============================================================*/
INSERT INTO EQUIPO VALUES
(1,1,1,'Prestado'), /*ONU Prestado+*/
(2,2,1,'Prestado'); /*Router Prestado+*/

/*==============================================================*/ 
/*      Ingreso de datos en tabla SERVICIO                      */ 
/*==============================================================*/
INSERT INTO SERVICIO VALUES
(1,2,1,'Internet Fibra PB'),
(2,2,2,'Internet Fibra PF'),
(3,2,3,'Internet Fibra PP');

/*==============================================================*/ 
/*      Ingreso de datos en tabla EMPLEADO                      */ 
/*==============================================================*/
SET DATESTYLE TO 'European';
INSERT INTO EMPLEADO VALUES
(232,1,'1350144471','03/01/2019','Benjie Frank','Gonzalez Sacon','09/04/2001','e1350144471@live.uleam.edu.ec','0939636536'),
(233,2,'1718998758','03/01/2019','Leonardo Paul','Quiñones Campoverde','25/01/1998','e1718998758@live.uleam.edu.ec','0956414243'),
(234,1,'1315178051','03/01/2019','Welinton Alexander','Guerrero Zamora','22/12/2000','e1315178051@live.uleam.edu.ec','0965646469');

/*==============================================================*/ 
/*      Ingreso de datos en tabla CONTRATO                      */ 
/*==============================================================*/
SET DATESTYLE TO 'European';
INSERT INTO CONTRATO VALUES
(203040,1150,232,1,'01/01/2019','31/12/2020','Finalizado','Jocay','Pago Anual'),
(203041,1151,233,2,'02/01/2019','31/12/2020','Finalizado','4 de noviembre','Pago Anual'),
(203042,1152,234,1,'03/01/2019','31/12/2020','Finalizado','5 de junio','Pago Anual'),
(203043,1153,232,1,'01/01/2020','31/12/2021','Activo','4 de noviembre','Pago Anual'),
(203044,1153,234,2,'03/01/2020','31/12/2021','Activo','Miraflores','Pago Anual'),
(203045,1154,232,3,'05/01/2020','31/12/2021','Activo','4 de noviembre','Pago Anual'),
(203046,1155,234,1,'04/01/2020','31/12/2021','Activo','15 de Septiembre','Pago Anual'),
(203047,1151,233,2,'06/01/2020','31/12/2021','Activo','Jocay','Pago Anual'),
(203048,1152,232,1,'15/01/2020','31/12/2021','Activo','Jocay','Pago Anual'),
(203049,1153,234,1,'05/01/2021','31/12/2022','Activo','Miraflores','Pago Mensual'),
(203050,1150,232,3,'04/01/2021','31/12/2022','Activo','4 de noviembre','Pago Mensual'),
(203051,1151,233,2,'05/01/2021','31/12/2022','Activo','Miraflores','Pago Mensual');

/*==============================================================*/ 
/*      Ingreso de datos en tabla CUENTA_BANCARIA                */ 
/*==============================================================*/
INSERT INTO CUENTA_BANCARIA VALUES
(1,1,203040,'1548749844'),
(2,3,203041,'1547468844'),
(3,3,203042,'1548749884'),
(4,1,203043,'1548748798'),
(5,2,203044,'1548788982'),
(6,1,203045,'1548752264'),
(7,2,203046,'1547468454'),
(8,3,203047,'1646868458'),
(9,1,203048,'1548465465'),
(10,1,203049,'4454512147'),
(11,2,203050,'5164132158'),
(12,1,203051,'2131864878');

/*==============================================================*/ 
/*      Ingreso de datos en tabla  FACTURA                      */ 
/*==============================================================*/
SET DATESTYLE TO 'European';
ALTER TABLE FACTURA ALTER COLUMN FACTURA_TOTAL SET DATA TYPE decimal;
INSERT INTO FACTURA VALUES
/*   Facturas Anuales   */ 
(1,1,1,'31/12/2019',342.00),
(2,1,1,'31/12/2020',342.00),
(3,2,1,'31/12/2019',471.60),
(4,2,1,'31/12/2020',471.60),
(5,3,1,'31/12/2019',342.00),
(6,3,1,'31/12/2020',342.00),
(7,4,1,'31/12/2020',342.00),
(8,5,1,'31/12/2020',471.60),
(9,6,1,'31/12/2020',833.40),
(10,7,1,'31/12/2020',342.00),
(11,8,1,'31/12/2020',471.60),
(12,9,1,'31/12/2020',342.00),

/*   Facturas Mensuales   */ 
(13,10,1,'05/02/2021',28.50),
(14,10,2,'05/03/2021',28.50),
(15,10,1,'05/04/2021',28.50),
(16,10,1,'05/05/2021',28.50),
(17,10,1,'05/06/2021',28.50),
(18,10,1,'05/07/2021',28.50),
(19,10,1,'05/08/2021',28.50),
(20,10,1,'05/09/2021',28.50),
(21,10,1,'05/10/2021',28.50),
(22,10,1,'05/11/2021',28.50),

/*Factura Men con 2 Reconexiones*/ 
(23,11,1,'04/02/2021',69.45),
(24,11,2,'04/03/2021',69.45),
(25,11,1,'04/04/2021',69.45),
(26,11,1,'04/05/2021',69.45),
(27,11,1,'04/06/2021',69.45),
(28,11,1,'04/07/2021',69.45),
(29,11,1,'04/08/2021',69.45),
(30,11,2,'04/09/2021',69.45),
(31,11,1,'04/10/2021',69.45),
(32,11,1,'04/11/2021',69.45),

/*Factura Men con 2 Reconexiones*/ 
(33,12,1,'05/02/2021',39.30),
(34,12,1,'05/03/2021',39.30),
(35,12,1,'05/04/2021',39.30),
(36,12,2,'05/05/2021',39.30),
(37,12,1,'05/06/2021',39.30),
(38,12,2,'05/07/2021',39.30),
(39,12,1,'05/08/2021',39.30),
(40,12,1,'05/09/2021',39.30),
(41,12,1,'05/10/2021',39.30),
(42,12,1,'05/11/2021',39.30);
