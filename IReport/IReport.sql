/*==============================================================*/ 
/*  IReport: Ingresos por año en la empresa en diagrama de barras.*/ 
/*==============================================================*/
SELECT EXTRACT (YEAR FROM FECHA_PAGADO) AS PERIODO, SUM (FACTURA_TOTAL) AS INGRESO_TOTAL FROM FACTURA
GROUP BY EXTRACT (YEAR FROM FECHA_PAGADO) 
ORDER BY PERIODO ASC